import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

val title = "GroceriesService"
group = "it.kegel"
version = "0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_1_8

plugins {
    id("org.springframework.boot") version "2.1.13.RELEASE"
    id("io.spring.dependency-management") version "1.0.9.RELEASE"
    kotlin("jvm") version "1.3.40"
    kotlin("plugin.spring") version "1.3.40"
    kotlin("plugin.jpa") version "1.3.40"
    application
    jacoco
}

repositories {
    mavenCentral()
    jcenter()
}

val test by tasks.getting(Test::class) {
    useJUnitPlatform {}

    testLogging.showStandardStreams = true
}

dependencies {
    implementation(kotlin("stdlib"))
    implementation(kotlin("reflect:1.3.40"))
    implementation("org.springframework.boot:spring-boot-starter-data-jpa")
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.springframework.boot:spring-boot-starter-security")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("com.auth0:java-jwt:3.4.0")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.2.2")
    implementation("com.beust:klaxon:5.0.12")
    implementation("com.amazonaws:aws-java-sdk-secretsmanager:1.11.549")
    implementation("io.springfox:springfox-swagger2:2.9.2")
    implementation("io.springfox:springfox-swagger-ui:2.9.2")
    implementation("io.springfox:springfox-bean-validators:2.9.2")
    implementation("org.apache.logging.log4j:log4j-core:2.16.0")

    runtimeOnly("mysql:mysql-connector-java:5.1.49")

    testImplementation("org.springframework.boot:spring-boot-starter-test") {
        exclude(group = "org.junit.vintage", module = "junit-vintage-engine")
        exclude(module = "mockito-core")
    }
    testImplementation("org.junit.jupiter:junit-jupiter-api")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine")
    testImplementation("com.ninja-squad:springmockk:1.1.3")
    testImplementation(group = "org.apache.logging.log4j", name = "log4j-core", version = "2.16.0", classifier = "tests")
    testRuntimeOnly("com.h2database:h2")
}

tasks {
    getByName<JacocoReport>("jacocoTestReport") {
        afterEvaluate {
            getClassDirectories().setFrom(files(classDirectories.files.map {
                fileTree(it) { exclude("**/ui/**") }
            }))
        }
    }
    withType<KotlinCompile> {
        kotlinOptions {
            freeCompilerArgs = listOf("-Xjsr305=strict")
            jvmTarget = "1.8"
        }
    }
    bootJar {
        manifest {
            attributes(
                    "Implementation-Title" to title,
                    "Implementation-Version" to archiveVersion
            )
        }
    }
}

jacoco {
    toolVersion = "0.8.3"
}

application {
    mainClassName = "it.kegel.base.groceries.GroceriesApplicationKt"
}

defaultTasks("clean", "test", "jacocoTestReport")

