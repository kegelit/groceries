package it.kegel.base.groceries

import com.fasterxml.jackson.databind.ObjectMapper
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.context.event.EventListener
import org.springframework.http.HttpHeaders
import org.springframework.http.ResponseEntity
import org.springframework.scheduling.annotation.Async
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter
import java.util.*
import java.util.concurrent.CopyOnWriteArrayList
import java.util.function.Consumer
import javax.servlet.http.HttpServletResponse


@RestController
@RequestMapping("/stream")
@Api(value="Groceries", description="Operations for getting streams")
class EventController {

//    var x = 0
//
//    @GetMapping
//    @ApiOperation(value = "Get stream")
//    fun stream(): ResponseEntity<Any> {
//        return ResponseEntity.ok()
//                .headers( headers())
//                .body("data:Event" + x++ + "\n\n")
//    }
//
//    private fun headers(): HttpHeaders {
//        val headers = HttpHeaders()
//        headers.add("Content-Type", "text/event-stream;charset=UTF-8")
//        return headers
//    }

    private val emitters = CopyOnWriteArrayList<SseEmitter>()

    @GetMapping
    fun handle(response: HttpServletResponse): SseEmitter {
        response.setHeader("Cache-Control", "no-store")
        val emitter = SseEmitter()
        emitters.add(emitter)
        emitter.onCompletion { emitters.remove(emitter) }
        emitter.onTimeout { emitters.remove(emitter) }
        return emitter
    }

    @Async
    @EventListener
    fun onNewTask(event: NewTaskEvent) {
        println("Received new task event - " + event.task.id)
        val deadEmitters: MutableList<SseEmitter> = ArrayList()
        emitters.forEach(Consumer { emitter: SseEmitter ->
            try {
                val mapper = ObjectMapper()
                val data = mapper.writeValueAsString(event.task)
                emitter.send(data)
            } catch (e: Exception) {
                deadEmitters.add(emitter)
            }
        })
        emitters.removeAll(deadEmitters)
    }

    @EventListener
    fun onEvent(event: CustomSpringEvent) {
        println("Received spring custom event - " + event.message)
        val deadEmitters: MutableList<SseEmitter> = ArrayList()
        emitters.forEach(Consumer { emitter: SseEmitter ->
            try {
                emitter.send(event.message)

                // close connnection, browser automatically reconnects
                // emitter.complete();

                // SseEventBuilder builder = SseEmitter.event().name("second").data("1");
                // SseEventBuilder builder =
                // SseEmitter.event().reconnectTime(10_000L).data(memoryInfo).id("1");
                // emitter.send(builder);
            } catch (e: Exception) {
                deadEmitters.add(emitter)
            }
        })
        emitters.removeAll(deadEmitters)
    }


}