package it.kegel.base.groceries.tasklists

import io.swagger.annotations.*
import org.springframework.http.ResponseEntity
import org.springframework.security.core.Authentication
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@RequestMapping("/lists")
@Api(value="Lists", description="Operations for managing lists")
class TaskListController(val service: TaskListService) {

    @GetMapping
    @ApiOperation(value = "View an overview of lists")
    @ApiResponses(
            ApiResponse(code = 200, message = "The overiew os lists was successfully retrieved")
    )
    fun lists(authentication: Authentication): ResponseEntity<Iterable<TaskList>> {
        val userId = getUserIdFromAuthenticationToken(authentication)
        return ResponseEntity.ok(service.getAll(userId))
    }

    @PostMapping()
    @ApiOperation(value = "Create a list")
    @ApiResponses(
            ApiResponse(code = 200, message = "The list was successfully added")
    )
    fun create(authentication: Authentication, @RequestBody createRequest: CreateTaskListRequest): ResponseEntity<Any> {
        val userId = getUserIdFromAuthenticationToken(authentication)
        val taskList = TaskList(createRequest.name, userId, createRequest.id)
        service.save(taskList)
        return ResponseEntity.ok(taskList)
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete a list")
    @ApiResponses(
            ApiResponse(code = 204, message = "The list does not exist anymore")
    )
    fun delete(@ApiParam(value = "List ID", required = true) @PathVariable id: UUID): ResponseEntity<Any> {
        service.delete(id)
        return ResponseEntity.noContent().build();
    }

    private fun getUserIdFromAuthenticationToken(authentication: Authentication) : UUID{
        val details = authentication.details as Map<String, String>
        return UUID.fromString(details["id"])
    }

    data class CreateTaskListRequest(
            @ApiModelProperty(notes = "The name of the TaskList")
            val name: String,

            @ApiModelProperty(notes = "The ID of the TaskList")
            val id: UUID = UUID.randomUUID()
    )
}