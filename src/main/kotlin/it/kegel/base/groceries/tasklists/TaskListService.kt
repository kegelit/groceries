package it.kegel.base.groceries.tasklists

import it.kegel.base.groceries.tasklists.TaskList
import it.kegel.base.groceries.tasklists.TaskListRepository
import org.springframework.stereotype.Service
import java.util.*
import javax.transaction.Transactional

@Service
@Transactional
class TaskListService(val repository: TaskListRepository) {

    fun getAll(): Iterable<TaskList> {
        return repository.findAll()
    }

    fun getAll(userId: UUID): Iterable<TaskList> {
        return repository.findTaskListByUserId(userId)
    }

    fun save(taskList: TaskList) {
        repository.save(taskList)
    }

    fun delete(id: UUID) {
        repository.deleteById(id)
    }
}