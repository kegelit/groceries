package it.kegel.base.groceries.tasklists

import it.kegel.base.groceries.tasklists.TaskList
import org.springframework.data.repository.CrudRepository
import java.util.*

interface TaskListRepository : CrudRepository<TaskList, UUID> {
    fun findTaskListByUserId(userId: UUID): List<TaskList>
}