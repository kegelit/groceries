package it.kegel.base.groceries.tasklists

import com.fasterxml.jackson.annotation.JsonIgnore
import io.swagger.annotations.ApiModelProperty
import it.kegel.base.groceries.UUIDConverter
import java.util.*
import javax.persistence.*


@Entity
data class TaskList(
        @ApiModelProperty(notes = "The name of the TaskList")
        val name: String,

        @ApiModelProperty(notes = "The userId of the TaskList")
        @Column(columnDefinition = "BINARY(16)")
        @JsonIgnore
        @Convert(converter = UUIDConverter::class)
        private val userId: UUID = UUID.fromString("00000000-0000-0000-0000-000000000000"),

        @ApiModelProperty(notes = "The ID of the TaskList")
        @Id
        @Column(columnDefinition = "BINARY(16)")
        val id: UUID = UUID.randomUUID()
)