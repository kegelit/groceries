package it.kegel.base.groceries

import it.kegel.base.groceries.security.UnauthorizedException
import it.kegel.base.groceries.tasks.UnknownTask
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.http.converter.HttpMessageNotReadableException
import org.springframework.web.bind.MissingRequestHeaderException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.context.request.WebRequest
import java.lang.IllegalArgumentException


@ControllerAdvice
class GlobalExceptionHandler {
    val logger: Logger = LogManager.getLogger(GlobalExceptionHandler::class);

    @ExceptionHandler(ResourceNotFound::class)
    @ResponseStatus(value=HttpStatus.NOT_FOUND)
    fun resourceNotFoundException(ex: ResourceNotFound, request: WebRequest): ResponseEntity<*> {
        logger.error("Error in ${request.getDescription(false)}", ex);
        return ResponseEntity<ErrorDetails>(ErrorDetails("Not found"), HttpStatus.NOT_FOUND)
    }

    @ExceptionHandler(UnknownTask::class)
    @ResponseStatus(value=HttpStatus.NOT_FOUND)
    fun unknownTaskException(ex: UnknownTask, request: WebRequest): ResponseEntity<*> {
        logger.error("Error in ${request.getDescription(false)}");
        return ResponseEntity<ErrorDetails>(ErrorDetails("Not found"), HttpStatus.NOT_FOUND)
    }

    @ExceptionHandler(IllegalArgumentException::class, HttpMessageNotReadableException::class)
    fun illegalArgumentExcepetionHandler(ex: Exception, request: WebRequest): ResponseEntity<*> {
        logger.error("Error in ${request.getDescription(false)} because ${ex.message ?: "(unknown)"}");
        return ResponseEntity<Any>(HttpStatus.BAD_REQUEST)
    }

    @ExceptionHandler(MissingRequestHeaderException::class)
    fun missingRequestHeaderExceptionHandler(ex: Exception, request: WebRequest): ResponseEntity<*> {
        logger.error("Error in ${request.getDescription(false)} because ${ex.message ?: "(unknown)"}");
        return ResponseEntity<ErrorDetails>(ErrorDetails(ex.message ?: "Bad Request"), HttpStatus.BAD_REQUEST)
    }

    @ExceptionHandler(UnauthorizedException::class)
    fun unauthorizedExceptionHandler(ex: UnauthorizedException, request: WebRequest): ResponseEntity<*> {
        logger.error("Error in ${request.getDescription(false)} because ${ex.message ?: "(unknown)"}");
        return ResponseEntity<ErrorDetails>(ErrorDetails(ex.message ?: "Unauthorized"), HttpStatus.UNAUTHORIZED)
    }

    @ExceptionHandler(Exception::class)
    fun globalExcepetionHandler(ex: Exception, request: WebRequest): ResponseEntity<*> {
        logger.error("Error in ${request.getDescription(false)}", ex);
        return ResponseEntity<Any>(HttpStatus.INTERNAL_SERVER_ERROR)
    }
}