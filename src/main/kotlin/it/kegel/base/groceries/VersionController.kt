package it.kegel.base.groceries

import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import it.kegel.base.Manifest
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationEventPublisher
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController


@RestController
@RequestMapping("/version")
@Api(value="Groceries", description="Operations for showing version")
class VersionController(val applicationEventPublisher: ApplicationEventPublisher) {

    @GetMapping
    @ApiOperation(value = "View version")
    fun version() = ResponseEntity.ok(readVersion())

    private fun readVersion(): String {
        doStuffAndPublishAnEvent("My event")
        val optionalVersion = Manifest.getValue("Implementation-Version")
        return if (optionalVersion.isPresent) optionalVersion.get() else "0.0"
    }

    private fun doStuffAndPublishAnEvent(message: String) {
        println("Publishing custom event. ")
        val customSpringEvent = CustomSpringEvent(this, message)
        applicationEventPublisher.publishEvent(customSpringEvent)
    }
}