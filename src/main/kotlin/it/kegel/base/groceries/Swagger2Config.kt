package it.kegel.base.groceries

import it.kegel.base.Manifest
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.config.annotation.EnableWebMvc
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer
import springfox.documentation.builders.ApiInfoBuilder
import springfox.documentation.builders.ParameterBuilder
import springfox.documentation.builders.PathSelectors
import springfox.documentation.builders.RequestHandlerSelectors
import springfox.documentation.schema.ModelRef
import springfox.documentation.service.ApiInfo
import springfox.documentation.service.Contact
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spring.web.plugins.Docket
import springfox.documentation.swagger.web.UiConfiguration
import springfox.documentation.swagger.web.UiConfigurationBuilder
import springfox.documentation.swagger2.annotations.EnableSwagger2


@Configuration
@EnableSwagger2
//@EnableWebMvc
class Swagger2Config {
    @Bean
    fun api(): Docket {

        val jwtHeader = ParameterBuilder()
                .name("Authorization")
                .description("JWT Token")
                .pattern("Bearer <access_token>")
                .modelRef(ModelRef("string"))
                .parameterType("header")
                .required(false)
                .build()
        val tokenHeader = ParameterBuilder()
                .name("X-Token")
                .description("User ID")
                .pattern("00000000-0000-0000-0000-000000000000")
                .modelRef(ModelRef("string"))
                .parameterType("header")
                .required(false)
                .build()
        val aParameters = listOf(jwtHeader, tokenHeader)

        return Docket(DocumentationType.SWAGGER_2).select()
                .apis(RequestHandlerSelectors.basePackage("it.kegel.base.groceries"))
                .paths(PathSelectors.regex("/.*"))
                .build().apiInfo(apiEndPointsInfo())
                .globalOperationParameters(aParameters)
    }

    @Bean
    fun webMvcConfigurer(): WebMvcConfigurer? {
        return object : WebMvcConfigurer {
            override fun addResourceHandlers(registry: ResourceHandlerRegistry) {
                registry.addResourceHandler("swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/")
            }
        }
    }

    @Bean
    fun uiConfig(): UiConfiguration {
        return UiConfigurationBuilder.builder()
                .displayRequestDuration(true)
                .validatorUrl("")
                .build();
    }

    private fun apiEndPointsInfo(): ApiInfo {
        return ApiInfoBuilder().title("Groceries REST API")
                .description("Groceries REST API")
                .contact(Contact("Gerben Kegel", "https://www.kegel.it", "gerben@kegel.it"))
                .license("Apache 2.0")
                .licenseUrl("http://www.apache.org/licenses/LICENSE-2.0.html")
                .version(readVersion())
                .build()
    }

    private fun readVersion(): String {
        val optionalVersion = Manifest.getValue("Implementation-Version")
        return if (optionalVersion.isPresent) optionalVersion.get() else "0.0"
    }
}