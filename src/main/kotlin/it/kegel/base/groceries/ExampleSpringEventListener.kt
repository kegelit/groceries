package it.kegel.base.groceries

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.springframework.context.ApplicationListener
import org.springframework.context.event.EventListener
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Component

@Component
class ExampleSpringEventListener {

    @Async
    @EventListener
    fun onApplicationEvent(event: CustomSpringEvent) {
        Thread.sleep(4000)
//        GlobalScope.launch {
//            delay(2000)
            println("Received spring custom event - " + event.message)
//        }

    }
}