package it.kegel.base.groceries

import java.lang.System.arraycopy
import java.util.*
import javax.persistence.AttributeConverter
import javax.persistence.Converter


/*
 * Auto apply this converter to all UUID-based attributes, excluding the list
 * of exceptions mentioned in the JPA spec.
 */
@Converter(autoApply = true)
class UUIDConverter : AttributeConverter<UUID, ByteArray> {
    override fun convertToDatabaseColumn(uuid: UUID): ByteArray {
        val out = ByteArray(16)
        val msbIn: ByteArray = fromLong(uuid.mostSignificantBits)

        // Reorder the UUID, swapping the hi- and low- digits of the timestamp
        arraycopy(msbIn, 6, out, 0, 2)
        arraycopy(msbIn, 4, out, 2, 2)
        arraycopy(msbIn, 0, out, 4, 4)

        // The remainder of the UUID (clock sequence and MAC address) are kept as-is
        arraycopy(fromLong(uuid.leastSignificantBits), 0, out, 8, 8)
        return out
    }

    override fun convertToEntityAttribute(value: ByteArray): UUID {
        val msb = ByteArray(8)
        val lsb = ByteArray(8)

        // Reorder the UUID, swapping the hi- and low- digits of the timestamp
        arraycopy(value, 4, msb, 0, 4)
        arraycopy(value, 2, msb, 4, 2)
        arraycopy(value, 0, msb, 6, 2)

        // The remainder of the UUID (clock sequence and MAC address) are kept as-is
        arraycopy(value, 8, lsb, 0, 8)
        return UUID(asLong(msb), asLong(lsb))
    }

    /**
     * Interpret a long as its binary form
     *
     * @param longValue The long to interpret to binary
     *
     * @return The binary
     */
    fun fromLong(longValue: Long): ByteArray {
        val bytes = ByteArray(8)
        fromLong(longValue, bytes, 0)
        return bytes
    }

    /**
     * Interpret a long as its binary form
     *
     * @param longValue The long to interpret to binary
     * @param dest the destination array.
     * @param destPos starting position in the destination array.
     * @return The binary
     */
    fun fromLong(longValue: Long, dest: ByteArray, destPos: Int) {
        dest[destPos] = (longValue shr 56).toByte()
        dest[destPos + 1] = (longValue shl 8 shr 56).toByte()
        dest[destPos + 2] = (longValue shl 16 shr 56).toByte()
        dest[destPos + 3] = (longValue shl 24 shr 56).toByte()
        dest[destPos + 4] = (longValue shl 32 shr 56).toByte()
        dest[destPos + 5] = (longValue shl 40 shr 56).toByte()
        dest[destPos + 6] = (longValue shl 48 shr 56).toByte()
        dest[destPos + 7] = (longValue shl 56 shr 56).toByte()
    }

    fun asLong(bytes: ByteArray): Long {
        var value: Long = 0
        for (i in 0 until bytes.size) {
            value = (value shl 8) + (bytes[i].toLong() and 0xff)
        }
        return value
    }
}