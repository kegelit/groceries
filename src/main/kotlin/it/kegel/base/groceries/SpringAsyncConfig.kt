package it.kegel.base.groceries

import org.springframework.context.annotation.Configuration
import org.springframework.scheduling.annotation.EnableAsync

@Configuration
@EnableAsync
class SpringAsyncConfig {  }
