package it.kegel.base.groceries.users

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.*

@Service
@Transactional
class UserService(val repository: UserRepository, val bCryptPasswordEncoder: BCryptPasswordEncoder) {

    fun login(username: String, password: String): Boolean {
        val user = repository.findByUsername(username)
        return user != null && user.password == bCryptPasswordEncoder.encode(password)
    }

    fun register(username: String, password: String) {
        val user = User(username, bCryptPasswordEncoder.encode(password))
        repository.save(user)
    }

    fun register(id: UUID) {
        val user = User(id)
        repository.save(user)
    }
}