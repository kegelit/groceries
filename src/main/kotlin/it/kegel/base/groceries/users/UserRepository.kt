package it.kegel.base.groceries.users

import it.kegel.base.groceries.users.User
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface UserRepository : JpaRepository<User, UUID> {
    fun findByUsername(username: String): User?
}