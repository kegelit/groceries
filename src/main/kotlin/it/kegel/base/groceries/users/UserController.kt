package it.kegel.base.groceries.users

import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import it.kegel.base.groceries.ErrorDetails
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.lang.Exception
import java.util.*

@RestController
@RequestMapping("/")
@Api(value="Groceries", description="Operations for login in")
class UserController(val service: UserService) {

//    @PostMapping("/login")
//    @ApiOperation(value = "Login with credentials")
//    fun login(@RequestBody loginRequest: LoginRequest): ResponseEntity<Any> {
//        if (service.login(loginRequest.username, loginRequest.password)) {
//            val token = UUID.randomUUID().toString() //TODO change to actual JWT
//            return ResponseEntity.ok(LoginResponse(token));
//        } else {
//            return ResponseEntity(ErrorDetails("Invalid credentials"), HttpStatus.UNAUTHORIZED)
//        }
//    }

    @PostMapping("/register")
    @ApiOperation(value = "Register a named user")
    fun register(@RequestBody user: User): ResponseEntity<Any> {
        try {
            service.register(user.username, user.password)
            return ResponseEntity(HttpStatus.CREATED)
        } catch (ex: Exception) {
            return ResponseEntity(ErrorDetails(ex.message ?: ""), HttpStatus.BAD_REQUEST)
        }
    }

    @PostMapping("/register/{id}")
    @ApiOperation(value = "Register an anonymous user")
    fun registerById(@PathVariable id: UUID): ResponseEntity<Any> {
        try {
            service.register(id)
            return ResponseEntity(HttpStatus.CREATED)
        } catch (ex: Exception) {
            return ResponseEntity(ErrorDetails(ex.message ?: ""), HttpStatus.BAD_REQUEST)
        }
    }

    data class LoginRequest(val username: String, val password: String)

    data class LoginResponse(val token: String)
}
