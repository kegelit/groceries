package it.kegel.base.groceries.users

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id

@Entity
@ApiModel(description = "All details about the User.")
data class User(
        @ApiModelProperty(notes = "The username of the User")
        val username: String = "",

        @ApiModelProperty(notes = "The encrypted password of the Task")
        val password: String = "",

        @ApiModelProperty(notes = "The ID of the User")
        @Column(columnDefinition = "BINARY(16)")
        @Id
        val id: UUID = UUID.randomUUID()) {

    constructor(id: UUID) : this ("", "", id)
}