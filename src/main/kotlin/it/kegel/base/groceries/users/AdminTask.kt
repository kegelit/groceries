package it.kegel.base.groceries.users

import it.kegel.base.groceries.tasks.Task
import java.util.*

class AdminTask(val name: String,
                val listId: UUID,
                val id: UUID,
                val checked: Boolean) {

    constructor(task: Task) : this(task.name, task.listId, task.id, task.isChecked())

}