package it.kegel.base.groceries

import org.springframework.context.ApplicationEvent


class CustomSpringEvent(source: Any, val message: String) : ApplicationEvent(source)