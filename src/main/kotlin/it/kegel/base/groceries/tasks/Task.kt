package it.kegel.base.groceries.tasks

import com.fasterxml.jackson.annotation.JsonIgnore
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import it.kegel.base.groceries.UUIDConverter
import java.util.*
import javax.persistence.Column
import javax.persistence.Convert
import javax.persistence.Entity
import javax.persistence.Id

@Entity
@ApiModel(description = "All details about the Task.")
data class Task(
        @ApiModelProperty(notes = "The name of the Task")
        val name: String,

        @ApiModelProperty(notes = "The ID of the TaskList")
        @Column(columnDefinition = "BINARY(16)")
        @JsonIgnore
        @Convert(converter = UUIDConverter::class)
        var listId: UUID = UUID.fromString("00000000-0000-0000-0000-000000000000"),

        @ApiModelProperty(notes = "The ID of the Task")
        @Column(columnDefinition = "BINARY(16)")
        @Id
        val id: UUID = UUID.randomUUID()) {

    @ApiModelProperty(notes = "Whether this task is checked")
    private var checked = false

    fun isChecked() = checked

    fun check(): Unit {
        checked = true
    }

    fun uncheck(): Unit {
        checked = false
    }
}
