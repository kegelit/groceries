package it.kegel.base.groceries.tasks

import it.kegel.base.groceries.users.AdminTask
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/admin")
class AdminController(val service: TaskService) {

    @GetMapping("/tasks")

    fun tasks(): ResponseEntity<Iterable<AdminTask>> {
        return ResponseEntity.ok(service.getAll().get().map{ task -> AdminTask(task) })
    }
}