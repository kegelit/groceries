package it.kegel.base.groceries.tasks

import io.swagger.annotations.*
import it.kegel.base.groceries.NewTaskEvent
import org.springframework.context.ApplicationEventPublisher
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@RequestMapping("/lists/{listId}/tasks")
@Api(value="Groceries", description="Operations for managing tasks")
class TaskController(val service: TaskService, val applicationEventPublisher: ApplicationEventPublisher) {

    @GetMapping
    @ApiOperation(value = "View a list of tasks")
    @ApiResponses(
            ApiResponse(code = 200, message = "The task list was successfully retrieved")
    )
    fun tasks(@PathVariable listId: UUID): ResponseEntity<Iterable<Task>> {
        return ResponseEntity.ok(service.getAllByListId(listId))
    }

    @PostMapping
    @ApiOperation(value = "Add a task")
    @ApiResponses(
            ApiResponse(code = 200, message = "The task was successfully added")
    )
    fun create(@PathVariable listId: UUID, @ApiParam(value = "Task definition", required = true) @RequestBody task: Task): ResponseEntity<Task> {
        val t = Task(task.name, listId, task.id)
        service.save(t)
        applicationEventPublisher.publishEvent(NewTaskEvent(this, t))

        return ResponseEntity.ok(t)
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Get a task")
    @ApiResponses(
            ApiResponse(code = 200, message = "The task"),
            ApiResponse(code = 404, message = "The task does not exist")
    )
    fun get(@PathVariable id: UUID) : ResponseEntity<Task> {
        return ResponseEntity.ok(service.get(id))
    }

    @PostMapping("/{id}/check")
    @ApiOperation(value = "Check a task")
    @ApiResponses(
            ApiResponse(code = 200, message = "The task was successfully checked"),
            ApiResponse(code = 404, message = "The task does not exist")
    )
    fun check(@ApiParam(value = "Task ID", required = true) @PathVariable id: UUID): ResponseEntity<Any> {
        try {
            val task = service.get(id)
            service.check(id)

            return ResponseEntity.ok(task)
        } catch (e: UnknownTask) {
            return ResponseEntity.notFound().build()
        }

    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete a task")
    @ApiResponses(
        ApiResponse(code = 204, message = "The task does not exist anymore")
    )
    fun delete(@ApiParam(value = "Task ID", required = true) @PathVariable id: UUID) : ResponseEntity<Void>  {
        service.delete(id)
        return ResponseEntity.noContent().build();
    }
}