package it.kegel.base.groceries.tasks

import java.util.*

class UnknownTask(id: UUID) : RuntimeException("Unknown id $id") {
}