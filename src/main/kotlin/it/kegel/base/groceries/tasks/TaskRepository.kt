package it.kegel.base.groceries.tasks

import it.kegel.base.groceries.tasks.Task
import org.springframework.data.repository.CrudRepository
import java.util.*

interface TaskRepository : CrudRepository<Task, UUID> {
    fun findAllByListId(listId: UUID) : Iterable<Task>
}