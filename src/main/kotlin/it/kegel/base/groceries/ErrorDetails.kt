package it.kegel.base.groceries

data class ErrorDetails(val message: String)