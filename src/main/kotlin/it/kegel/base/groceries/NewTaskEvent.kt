package it.kegel.base.groceries

import it.kegel.base.groceries.tasks.Task
import org.springframework.context.ApplicationEvent

class NewTaskEvent(source: Any, val task: Task) : ApplicationEvent(source)
