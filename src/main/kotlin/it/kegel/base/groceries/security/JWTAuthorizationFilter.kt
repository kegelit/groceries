package it.kegel.base.groceries.security

import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import it.kegel.base.groceries.users.UserRepository
import it.kegel.base.groceries.security.SecurityConstants.HEADER_STRING
import it.kegel.base.groceries.security.SecurityConstants.SECRET
import it.kegel.base.groceries.security.SecurityConstants.TOKEN_PREFIX
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter
import java.io.IOException
import java.util.*
import javax.servlet.FilterChain
import javax.servlet.ServletException
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


class JWTAuthorizationFilter(authManager: AuthenticationManager, val userRepository: UserRepository) : BasicAuthenticationFilter(authManager) {

    @Throws(IOException::class, ServletException::class)
    override fun doFilterInternal(req: HttpServletRequest, res: HttpServletResponse, chain: FilterChain) {
        val header = req.getHeader(HEADER_STRING)
        if (header == null || !header.startsWith(TOKEN_PREFIX)) {
            if (req.getHeader("X-Token") == null) {
                chain.doFilter(req, res)
                return
            }
        }
        val authentication = getAuthentication(req)
        SecurityContextHolder.getContext().authentication = authentication
        chain.doFilter(req, res)
    }

    private fun getAuthentication(request: HttpServletRequest): UsernamePasswordAuthenticationToken? {
        val token = request.getHeader(HEADER_STRING)
        if (token != null) {
            val token = JWT.require(Algorithm.HMAC512(SECRET.toByteArray()))
                    .build()
                    .verify(token.replace(TOKEN_PREFIX, ""))
            val username = token.subject
            val id = token.getClaim("id").asString()

            return if (username != null) {
                createAuthenticationToken(username, id)
            } else null
        }
        val id = request.getHeader("X-Token")
        if (id != null) {
            val user = userRepository.findById(UUID.fromString(id))
            return if (user.isPresent) {
                createAuthenticationToken(user.get().username, id)
            } else null
        }

        return null
    }

    private fun createAuthenticationToken(username: String, id: String) : UsernamePasswordAuthenticationToken {
        return UsernamePasswordAuthenticationToken(username, null, ArrayList()).apply {
            details = mapOf("id" to id)
        }
    }
}