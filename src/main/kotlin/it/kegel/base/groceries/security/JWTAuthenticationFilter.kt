package it.kegel.base.groceries.security

import com.auth0.jwt.JWT
import com.fasterxml.jackson.databind.ObjectMapper
import java.io.IOException
import java.util.*
import javax.servlet.FilterChain
import javax.servlet.ServletException
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

import com.auth0.jwt.algorithms.Algorithm.HMAC512
import it.kegel.base.groceries.users.User
import it.kegel.base.groceries.security.SecurityConstants.EXPIRATION_TIME
import it.kegel.base.groceries.security.SecurityConstants.HEADER_STRING
import it.kegel.base.groceries.security.SecurityConstants.SECRET
import it.kegel.base.groceries.security.SecurityConstants.TOKEN_PREFIX
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.AuthenticationException
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter


class JWTAuthenticationFilter(private val authManager: AuthenticationManager) : UsernamePasswordAuthenticationFilter() {

    @Throws(AuthenticationException::class)
    override fun attemptAuthentication(req: HttpServletRequest, res: HttpServletResponse?): Authentication {
        return try {
            val user = ObjectMapper()
                    .readValue(req.inputStream, User::class.java)
            authManager.authenticate(
                    UsernamePasswordAuthenticationToken(
                            user.username,
                            user.password,
                            ArrayList()).apply {
                        details = mapOf("id" to user.id.toString())
                    }
            )
        } catch (e: IOException) {
            throw RuntimeException(e)
        }
    }

    @Throws(IOException::class, ServletException::class)
    override fun successfulAuthentication(req: HttpServletRequest?,
                                           res: HttpServletResponse,
                                           chain: FilterChain?,
                                           auth: Authentication) {
        val token = JWT.create()
                .withSubject((auth.getPrincipal() as org.springframework.security.core.userdetails.User).getUsername())
                .withExpiresAt(Date(System.currentTimeMillis() + EXPIRATION_TIME))
        val details = auth.details
        if (details is Map<*, *> && details.containsKey("id")) {
            token.withClaim("id", details["id"].toString())
        }

        res.addHeader(HEADER_STRING, TOKEN_PREFIX + token.sign(HMAC512(SECRET.toByteArray())))
    }
}