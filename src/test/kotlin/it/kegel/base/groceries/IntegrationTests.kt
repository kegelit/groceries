package it.kegel.base.groceries

import it.kegel.base.groceries.tasklists.TaskList
import it.kegel.base.groceries.tasklists.TaskListController
import it.kegel.base.groceries.tasks.Task
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.test.web.client.getForEntity
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.*
import java.util.*


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class IntegrationTests(@Autowired val restTemplate: TestRestTemplate) {

    private val headers = HttpHeaders()

    @BeforeAll
    internal fun beforeAll() {
        val id = "00000000-0000-0000-0000-000000001000"
        headers.set("X-Token", id)
        register(UUID.fromString(id))
    }

    @BeforeEach
    internal fun beforeEach() {
        removeAllTaskLists()
    }

    @Test
    fun testVersion() {
        val response = restTemplate.getForEntity<String>("/version")
        assertThat(response.statusCode).isEqualTo(HttpStatus.OK)
        assertThat(response.body).isEqualTo("2.9.10")
    }

    @Test
    fun testGetEmptyTaskList() {
        val response = getTaskList()
        assertThat(response.statusCode).isEqualTo(HttpStatus.OK)
        assertThat(response.body?.isEmpty()).isTrue()
    }

    @Test
    fun testCreateTaskList() {
        val listName = "Groceries"
        val response = createTaskList(listName)
        assertThat(response.statusCode).isEqualTo(HttpStatus.OK)
        assertThat(response.body?.name).isEqualTo(listName)
    }

    @Test
    fun testGetOneTaskList() {
        val listName = "Groceries"
        createTaskList(listName)
        val response = getTaskList()
        assertThat(response.statusCode).isEqualTo(HttpStatus.OK)
        assertThat(response.body?.size).isEqualTo(1)
        assertThat(response.body?.get(0)?.name).isEqualTo(listName)
    }

    @Test
    fun testGetTwoTaskLists() {
        createTaskList("Groceries")
        createTaskList("Shoppinglist")
        val response = getTaskList()
        assertThat(response.statusCode).isEqualTo(HttpStatus.OK)
        assertThat(response.body?.size).isEqualTo(2)
    }

    @Test
    fun testAddTaskToEmptyTaskList() {
        val task = "Eggs"
        val listId = createTaskList("Groceries").body?.id ?: UUID.fromString("00000000-0000-0000-0000-000000000000")
        val response = createTask(task, listId);
        assertThat(response.statusCode).isEqualTo(HttpStatus.OK)
        assertThat(response.body?.name).isEqualTo(task)
    }

    @Test
    fun testGetTaskListWithSingleTask() {
        val task = "Eggs"
        val listId = createTaskList("Groceries").body?.id ?: UUID.fromString("00000000-0000-0000-0000-000000000000")
        createTask(task, listId);

        val request = HttpEntity(null, headers)
        val response = restTemplate.exchange("/lists/$listId/tasks", HttpMethod.GET, request, object : ParameterizedTypeReference<List<Task>>(){})

        assertThat(response.statusCode).isEqualTo(HttpStatus.OK)
        assertThat(response.body?.size).isEqualTo(1)
        assertThat(response.body?.get(0)?.name).isEqualTo(task)
    }

    @Test
    fun testGetTaskListWithMultipleTasks() {
        val listId = createTaskList("Groceries").body?.id ?: UUID.fromString("00000000-0000-0000-0000-000000000000")
        createTask("Eggs", listId);
        createTask("Bread", listId);

        val request = HttpEntity(null, headers)
        val response = restTemplate.exchange("/lists/$listId/tasks", HttpMethod.GET, request, object : ParameterizedTypeReference<List<Task>>(){})

        assertThat(response.statusCode).isEqualTo(HttpStatus.OK)
        assertThat(response.body?.size).isEqualTo(2)
    }

    private fun getTaskList() : ResponseEntity<List<TaskList>> {
        val request = HttpEntity(null, headers)
        return restTemplate.exchange("/lists", HttpMethod.GET, request, object : ParameterizedTypeReference<List<TaskList>>(){})
    }

    private fun createTaskList(listName: String) : ResponseEntity<TaskList> {
        val request = HttpEntity(TaskListController.CreateTaskListRequest(listName), headers)
        return restTemplate.exchange("/lists", HttpMethod.POST, request, TaskList::class.java)
    }

    private fun createTask(name: String, listId: UUID) : ResponseEntity<Task> {
        val request = HttpEntity(Task(name, listId), headers)
        return restTemplate.exchange("/lists/$listId/tasks", HttpMethod.POST, request, Task::class.java)
    }

    private fun removeAllTaskLists() {
        getTaskList().body?.forEach{ taskList -> removeTaskList(taskList.id)}
    }

    private fun removeTaskList(listId: UUID) : ResponseEntity<String> {
        val request = HttpEntity(null, headers)
        return restTemplate.exchange("/lists/$listId", HttpMethod.DELETE, request, String::class.java)
    }

    private fun register(id: UUID) {
        val request = HttpEntity(null, headers)
        restTemplate.exchange("/register/$id", HttpMethod.POST, request, String::class.java)
    }
}