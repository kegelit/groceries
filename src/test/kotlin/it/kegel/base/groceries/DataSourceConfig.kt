package it.kegel.base.groceries

import com.amazonaws.services.secretsmanager.AWSSecretsManagerClientBuilder
import com.amazonaws.services.secretsmanager.model.*
import com.beust.klaxon.Klaxon
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.jdbc.DataSourceBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.io.File
import java.util.*
import javax.sql.DataSource
import kotlin.system.exitProcess


@Configuration
class DataSourceConfig() {

    @Bean
    fun dataSource(): DataSource {
        val username = "sa"
        val password = "sa"
        val url = "jdbc:h2:mem:db;DB_CLOSE_DELAY=-1"

        val dataSourceBuilder = DataSourceBuilder.create()
        dataSourceBuilder.driverClassName("org.h2.Driver")
        dataSourceBuilder.url(url)
        dataSourceBuilder.username(username)
        dataSourceBuilder.password(password)
        return dataSourceBuilder.build()
    }
}