package it.kegel.base

import it.kegel.base.groceries.UUIDConverter
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertTrue
import org.junit.jupiter.api.Test
import java.util.*

class UUIDTest {

    private val convertor = UUIDConverter()

    @Test
    fun testConvertLong() {
        val uuid = UUID.fromString("0a032baf-4e92-4bdd-a437-e93922c44a0a")
        val l = uuid.leastSignificantBits
        val bytes = convertor.fromLong(l)
        assertEquals(l, convertor.asLong(bytes))
    }

    @Test
    fun testConvertUUID() {
        val uuid = UUID.fromString("0a032baf-4e92-4bdd-a437-e93922c44a0a")
        val bytes = convertor.convertToDatabaseColumn(uuid)
        assertEquals(uuid, convertor.convertToEntityAttribute(bytes))
    }
}